//
//  Enum.swift
//  Enumerations
//
//  Created by Aleksey Knysh on 2/18/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

enum BurgerKing {
    case burger(name: String, cost: Double, foodAvailability: Bool)
    case cola(name: String, cost: Double, foodAvailability: Bool)
    case iceCream(name: String, cost: Double, foodAvailability: Bool)
    case milkShake(name: String, cost: Double, foodAvailability: Bool)
    case shrimpRoll(name: String, cost: Double, foodAvailability: Bool)
}

enum ServingSize {
    case smallSize
    case averageSize
    case bigSize
}
