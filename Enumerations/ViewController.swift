//
//  ViewController.swift
//  Enumerations
//
//  Created by Aleksey Knysh on 2/18/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var nameFood: String = ""
    var foodAvailability = Bool()
    var totalValue = 0.0
    var sum = 0.0
    var orderArray = [BurgerKing]()
    
    func randomSize() -> ServingSize {
        let all: [ServingSize] = [.averageSize, .bigSize, .smallSize]
        let randomIndex = Int(arc4random()) % all.count
        return all[randomIndex]
    }
    
    func reWithdrawal() {
        guard !foodAvailability == true else {
            return print("Извините! \(nameFood) - сегодня нет в наличии\n")
        }
        sum += totalValue
        print("\(nameFood) // Размер - \(randomSize()) // Стоимость - \(totalValue)\n")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let burger = BurgerKing.burger(name: "Бургер", cost: 7.4, foodAvailability: Bool.random())
        let cola = BurgerKing.cola(name: "Кола", cost: 2.5, foodAvailability: Bool.random())
        let iceCream = BurgerKing.iceCream(name: "Мороженное", cost: 4.3, foodAvailability: Bool.random())
        let milkShake = BurgerKing.milkShake(name: "Молочный напиток", cost: 3.7, foodAvailability: Bool.random())
        let shrimpRoll = BurgerKing.shrimpRoll(name: "Креветки", cost: 7.2, foodAvailability: Bool.random())
        
        // меню фаст-фуда
        let fastFood = [burger, cola, iceCream, milkShake, shrimpRoll]
        
        let number = Int.random(in: 1...4)
        for _ in 0...number {
            if let randomElement = fastFood.randomElement() {
                orderArray.append(randomElement)
            }
        }
        
        for count in orderArray {
            switch count {
            case .burger(let name, let cost, let availability): nameFood = name; totalValue = cost; foodAvailability = availability
            case .cola(let name, let cost, let availability): nameFood = name; totalValue = cost; foodAvailability = availability
            case .iceCream(let name, let cost, let availability): nameFood = name; totalValue = cost; foodAvailability = availability
            case .milkShake(let name, let cost, let availability): nameFood = name; totalValue = cost; foodAvailability = availability
            case .shrimpRoll(let name, let cost, let availability): nameFood = name; totalValue = cost; foodAvailability = availability
            }
            
            reWithdrawal()
        }
        
         print("Всего:\(sum)")
    }
}
